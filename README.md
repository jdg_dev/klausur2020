# Babel

Babel übersetzt es6 Code in browserkompatiblen JavaScrpt code.

Babel starten
 
```
node_modules/.bin/babel src/script.js -o script.js   
```

# Webpack

Fasst JavaScript Dateien anhand ihrer Abhängigkeiten zusammen.

Webpack starten

```
npx webpack
```

# Spannende Links

## Babel

### Browserkompatibiität

Für die Kompatibilität der generierten JS Dateien kann im Feld __target__ in der ```./.babelrc``` 
folgender Syntax verwendet werden:
https://github.com/browserslist/browserslist#full-list

### Presets

https://babeljs.io/docs/en/babel-preset-env

