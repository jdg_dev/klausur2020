const path = require('path');

module.exports = {
    mode: 'development',
    devtool: '',
    entry: ['./src/script.js'],
    output: {
        filename: 'script.min.js',
        path: path.resolve(__dirname, 'dist'),
    },
    module: {
        rules: [{
            test: /\.js/,
            exclude: /(node_modules|bower_components)/,
            use: 'babel-loader'
        }]
    },
};
