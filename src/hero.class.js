export default class Hero {
    constructor(name) {
        this.name = name;
        this.power = 'nothing';
    }

    speak() {
        console.log(`I am ${this.name} and my Superpower is ${this.power}!`)
    }
}
